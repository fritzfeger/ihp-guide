# IHP Guide: Working Examples

This repository is meant to accompany the [Guide for the Haskell framework IHP](https://ihp.digitallyinduced.com/Guide/). Feel free to raise an issue or submit a pull request!

The master branch is the example blog from the IHP Guide, as simple as possible and thus WITHOUT THE COMMENTING FUNCTIONALITY. Additional features described in the course of the Guide are available here one by one as feature branches, branched from master unless otherwise indicated. The branches are named in correspondence with the Guide headings and/or the URLs. Comments can be found in the README of the respective branch.
 
Changes to Fixtures.sql and Schema.sql are significant in most cases but can as well be irrelevant. Clone the repository and go to a feature branch to see it work. Usually, it is required to update the database after switching to another branch. In case of an error try once again.

The IHP version of each branch is usually the most recent at the time of creation of that branch. The master branch is usually updated before beginning a new branch because a new branch is routinely started by cloning the master. To be sure that a branch is run with the IHP version it is tested for, IHP should be rebuilt after checkout:

```nix
make clean
nix-shell -j auto --cores 0 --run 'make -B .envrc'
nix-shell --run 'make -B build/ihp-lib'
```

:white_check_mark:  branch ok  
:warning:  branch work in progress, partly running  
:x:  branch not running / critical errors  

### GETTING STARTED

:white_check_mark:  [master](https://gitlab.com/fritzfeger/ihp-guide/-/tree/master) without comments  
:white_check_mark:  [your-first-project](https://gitlab.com/fritzfeger/ihp-guide/-/tree/your-first-project) with comments  

### THE BASICS

:white_check_mark:  [scripts](https://gitlab.com/fritzfeger/ihp-guide/-/tree/scripts)  

#### Routing

:white_check_mark:  [routing-beautiful-urls](https://gitlab.com/fritzfeger/ihp-guide/-/tree/routing-beautiful-urls)  

#### Views & JSON

:white_check_mark: [view-seo](https://gitlab.com/fritzfeger/ihp-guide/-/tree/view-seo)  
:white_check_mark: [view-json](https://gitlab.com/fritzfeger/ihp-guide/-/tree/view-json)  

#### Forms

:white_check_mark: [form-select-inputs-custom-enums](https://gitlab.com/fritzfeger/ihp-guide/-/tree/form-select-inputs-custom-enums)
### ADVANCED

### FRONTEND

### DATABASE

:white_check_mark: [database-retrieve](https://gitlab.com/fritzfeger/ihp-guide/-/tree/database-retrieve)

### AUTH

:white_check_mark: [authentication](https://gitlab.com/fritzfeger/ihp-guide/-/tree/authentication)

### RECIPES

:white_check_mark: [static-pages](https://gitlab.com/fritzfeger/ihp-guide/-/tree/static-pages)  
:white_check_mark: [making-an-http-request](https://gitlab.com/fritzfeger/ihp-guide/-/tree/making-an-http-request)

### TROUBLESHOOTING


### NOT YET IN THE GUIDE

:white_check_mark: [syntax-highlighting](https://gitlab.com/fritzfeger/ihp-guide/-/tree/syntax-highlighting)
